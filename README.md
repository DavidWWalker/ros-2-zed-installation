1.0 INSTALLING ZED on TARGET Jetson FX1 - USB 3.0
=================================================================================================

1.0.1 USB 3.0

      USB 2.0 uses WHITE colored connectors and hubs 
      USB 3.0 uses BLUE  colored connectore and hubs
 
1.1 DOWNLOAD ZED_SDK  (V2.0.1) to /home/(user)/Downloads directory

    https://www.stereolabs.com/developers/release/2.0/#sdkdownloads_anchor

===
1.2 DOWNLOAD ZED_SDK_Linux-JTX1_v2.0.1.run to /home/(user)/Downloads

    $ ~cd\Downloads 
    $ sudo chmod +x ZED_SDK_Linus_JTX1_v2.0.1.run
    $ ./ZED_SDK_Linus_JTX1_v2.0.1.run
    ... installs to /usr/local/zed
    ... installs binary sample programs to /usr/local/zed/sample/bin
=== 

1.3 INSTALL QTCAM

    $ sudo apt-add-repository ppa:qtcam/xenial
    $ sudo apt-get update
    $ sudo apt-get install qtcam
    ... installed into /usr/bin/qtcam

1.4 INSTALL v4l2 Test Utility

    $ sudo apt-get update
    $ sudo apt-get install gem-plugin-v4l2

1.5 INSTALL V4l-utils

    $ sudo apt-get update
    $ sudo apt-get install v4l-utils

1.6 LIST USB DEVICES - $lsusb

	BUS 002 Device 005: ID  (Unknown Device)
	BUS 002 Device 004: ID  (Unknown Device)
	BUS 002 Device 003: ID  Genesys Logic, Inc
	BUS 002 Device 002: ID  NVidia Corp.
	BUS 002 Device 001: ID  Linux Foundation 3.0 root hub
	BUS 002 Device 005: ID  Microsoft Corp, Wired keyboard 400
	BUS 002 Device 004: ID  Logitech, Inc Unifying Reciever
	BUS 002 Device 003: ID  Genesys Logic, Inc 4 - port hub
	BUS 002 Device 001: ID  Linux Foundation 2.0 root hub

1.7 LIST VIDEO DEVICES REGISTERED WITH KERNEL - $ ls -ltrh /dev/video(all)

     /dev/video0
     /dev/video1   
     /dev/video2

1.8 LIST VIDEO DEVICES LINKED TO /DEV/VIDEO* - $ v4l2_ctl --lists

    Integrated_WebCam_HD - /dev/video2 - usv-00000:00:14.0-12
    See3CAM_130          - /dev/video0 - usb-tegra-xhci-2.2
    ZED                  - /dev/video1 - usb-regra-xhci-2.4   <---

1.9 EXECUTE QTCAM and ZED_SDK binary samples in /usr/local/zed/sample/bin

    Select Video Device to confirm camera is connected and configured correctly
    Confirm Resolution and FPS settings.
    ZED_Camera_Control
    ZED_Depth_Sensing
    ZED_Positional_Tracking

1.10 DOWNLOAD AND INSTALL (PCL) POINT CLOUD LIBARY needed by ROS

    sudo apt-get update
    sudo apt-get install libpcl-dev

1.11 INSTALL ZED WRAPPER FOR  ROS

    $ cd ~/catkin_ws 
 
1.11.1 ADD REPOSITORY TO WORKSPACE

    $ wstool set --git https://github.com/stereolabs/zed-ros-wrapper -t src  

9 - FAILURE


9 - FAILURE

    ubuntu@tegra-ubuntu:~/catkin_ws$ wstool set --git 
    https://github.com/stereolabs/zed-ros-wrapper -t src
    ERROR in config: /home/ubuntu/catkin_ws/src has no workspace configuration file '.rosinstall'
    
1.11.2  RETIEVE THE ZED-WRAPPER SOURCE CODE  

    $ wstool update -t src 

1.11.3 Build your workspace
 
    $ catkin build
 
1.11.4 Test ZED istallation
 
   $ roslaunch zed_wrapper zed.launch




2.0 INSTALLING ZED on HOST (Ubuntu 16.04) - USB 3.0
=================================================================================================
 


3.0 INSTALLING ZED on TARGET (J130) - USB 3.0
=================================================================================================